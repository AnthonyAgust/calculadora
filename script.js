document.addEventListener("DOMContentLoaded", function() {
    
    var inputA = document.getElementById("NA");
    var inputB = document.getElementById("NB");
  
    var resultadoSuma = document.getElementById("R_suma");
    var resultadoResta = document.getElementById("R_resta");
    var resultadoMultiplicacion = document.getElementById("R_multi");
    var resultadoDivision = document.getElementById("R_div");
    var resultadoModulo = document.getElementById("R_mod");
  
    var calcularButton = document.getElementById("calcular");
    calcularButton.addEventListener("click", calcular);
  
    function calcular() {

        var NA = parseFloat(inputA.value);
        var NB = parseFloat(inputB.value);
            
        if (!isNaN(NA) && !isNaN(NB)) {

            var suma = NA + NB;
            var resta = NA - NB;
            var multiplicacion = NA * NB;
            var division = NB !== 0 ? NA / NB : "Indefinido";
            var modulo = NB !== 0 ? NA % NB : "Indefinido";
            
            R_suma.innerHTML  = '<strong>Suma de A y B: </strong>' + suma;
            R_resta.innerHTML  = '<strong>Resta de A y B: </strong>' + resta;
            R_multi.innerHTML  = '<strong>Multiplicacion de A y B: </strong>' + multiplicacion;
            R_div.innerHTML  = '<strong>Division entre A y B: </strong>' + division;
            R_mod.innerHTML  = '<strong>Residuo de la division entre A y B: </strong>' + modulo;
            
            R_suma.style.display = "block";
            R_resta.style.display = "block";
            R_multi.style.display = "block";
            R_div.style.display = "block";
            R_mod.style.display = "block";
            
        } else {
    
            R_suma.style.display = "none";
            R_resta.style.display = "none";
            R_multi.style.display = "none";
            R_div.style.display = "none";
            R_mod.style.display = "none";
        }
    }


});
